# License Information

    yoko-guruma.jpg  
    yoko-wakare.jpg  

Author: Michael Hultström  
Year: 2013  
License: [CC BY-SA 2.0](https://creativecommons.org/licenses/by-sa/2.0/)  
Creative Commons Attribution-ShareAlike 2.0 Generic (CC BY-SA 2.0)  
Link: https://www.flickr.com/people/hultstrom/  

