# License Information

    amygdala.png  
    brainstem-model-fitted-frontal.png  
    brainstem-model-fitted-lateral.png  
    diencephalon-model-fitted-frontal.png  
    diencephalon-model-fitted-lateral.png  

Author: BodyParts3D,© The Database Center for Life Science (LSDB)  
Date: 19 September 2009  
License: [CC-BY-SA-2.1-jp](https://creativecommons.org/licenses/by-sa/2.1/jp/deed.en)  
Link: http://lifesciencedb.jp/  

> Mitsuhashi, N., Fujieda, K., Tamura, T., Kawamoto, S., Takagi, T., and Okubo, K.
(2009). « BodyParts3D: 3D Structure Database for Anatomical Concepts ». Nucleic
Acids Research 37(suppl 1), pp. D782–D785. doi: 10.1093/nar/gkn613



# License Information
    brainstem-drawing-clean-transparent.png  
    diencephalon-drawing-clean-transparency.png  
    long-reflexes--OpenStax.png  
    the-limbic-lobe--OpenStax.png  
    
Author: OpenStax College  
Textbook: Anatomy & Physiology  
Version from 2013  
License: Creative Commons Attribution License [(CC-BY 3.0)](https://creativecommons.org/licenses/by/3.0/)  
Link: https://legacy.cnx.org/content/col11496  

>  OpenStax College. (2013). Anatomy & Physiology. Retrieved from the OpenStax-CNX Web site: https://legacy.cnx.org/content/col11496
