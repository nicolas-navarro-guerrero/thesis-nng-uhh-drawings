#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
\title echo-state-dynamics.py
\brief emualtion of echo state property

 Version: 0.1
 Copyright (C) 2014 Nicolas Navarro-Guerrero (nng)
 Contact: nicolas.navarro.guerrero@gmail.com
          https://github.com/nicolas-navarro-guerrero/
 Created in: June 2014
 Last Modification by: Nicolas Navarro-Guerrero
 Last Modification in: June 2014

 LICENSE:
  "echo-state-dynamics"
  is free software: you can redistribute it and/or modify it under the terms of
  the GNU General Public License as published by the Free Software Foundation,
  either version 3 of the License, or (at your option) any later version.

  "echo-state-dynamics"
  is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
  PARTICULAR PURPOSE. See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  "echo-state-dynamics".
  If not, see <http://www.gnu.org/licenses/>.
"""
#******************************************************************************

#******************************************************************************
# Libraries Declaration
import numpy
import matplotlib.pyplot as plt 

#------------------------------------------------------------------------------
# Special libraries

#------------------------------------------------------------------------------
# Own Libraries

#******************************************************************************
# Global Variables Definition


#******************************************************************************
#******************************************************************************
def createEcho(t, ts, harmonics=0, shift=0):
    v = numpy.random.random_sample(numpy.shape(ts)) - 0.5;
    y = numpy.zeros(numpy.shape(ts));
    for idx in range(harmonics):
        y = numpy.sin(idx * numpy.pi * ts) + y;
    y = numpy.exp(-ts) * (y + v);
    y = y / numpy.max(numpy.abs(y));
    y = numpy.concatenate((numpy.zeros(shift), y));
    return y;

#******************************************************************************
def drawEcho(t, y):
    figure();
    plt.axes(frameon=False);
    plt.plot(t, y, 'k', linewidth=5);
    plt.axhline(linewidth=5, color='k', alpha=0.5)
    plt.axis((x_min, x_max, -1.1, 1.1));
    plt.tick_params(axis='both',          # changes apply to the x-axis
                which='both',      # both major and minor ticks are affected
                bottom='on',      # ticks along the bottom edge are off
                top='off',         # ticks along the top edge are off
                right='off',
                labelsize='20',
                width='3',
                labelbottom='on') # labels along the bottom edge are off)

#******************************************************************************
def saveEcho(echo, pathAndFile='/tmp/', basename='echo-state-', suffix='', fmt='svg'):
    plt.savefig(pathAndFile+basename+suffix+"."+fmt, 
                bbox_inches='tight', pad_inches=0.05);
    plt.savefig(pathAndFile+basename+suffix+"-transparent"+"."+fmt,
                bbox_inches='tight', transparent=True, pad_inches=0.05);
    
#******************************************************************************
#******************************************************************************
pathAndFile = "/tmp/";
basename = "echo-state-";
suffix = 'dirac';
fmt = 'svg';

lenght = 300;
x_min = 0
x_max = 5
t = numpy.linspace(x_min, x_max, lenght);

shift_factor = 0.15;
ts = t[0:300*(1-shift_factor)];
#print numpy.shape(us);

#u = numpy.zeros(numpy.shape(t));
#u[lenght*shift_factor] = 1;
#plt.plot(t, u, 'k', linewidth=5);
#ax1 = plt.axes(frameon=False);
#plt.axhline(linewidth=5, color='k', alpha=0.75)
#plt.axis((x_min, x_max, -1.1, 1.1));
#plt.tick_params(axis='both',          # changes apply to the x-axis
#                which='both',      # both major and minor ticks are affected
#                bottom='on',      # ticks along the bottom edge are off
#                top='off',         # ticks along the top edge are off
#                right='off',
#                labelsize='20',
#                width='3',
#                labelbottom='on') # labels along the bottom edge are off)
#plt.savefig(pathAndFile+basename+suffix+"-transparent"+"."+fmt,
#            bbox_inches='tight', transparent=True, pad_inches=0.05);

for idx in range(5):
    echo = createEcho(t, ts, harmonics=idx, shift=lenght*shift_factor);
    echo = drawEcho(t, echo);
    saveEcho(echo, suffix=str(idx));