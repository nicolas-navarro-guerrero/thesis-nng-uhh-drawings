# Source files of drawings and images included in my thesis

Version: 0.1  
Copyright (C) 2014-2016 [Nicolas Navarro-Guerrero](https://nicolas-navarro-guerrero.github.io)  
Contact:  nicolas.navarro.guerrero@gmail.com  

          https://github.com/nicolas-navarro-guerrero
          https://bitbucket.org/nicolas-navarro-guerrero      
          https://nicolas-navarro-guerrero.github.io

============================================================
### Overview ###
Most svg files have multiple layers and by hiding different combinations of these layers different figures were created. For instance, the svg file for chap 2 was also contains
the necessary information to produce the first figure of chap 7. Similarly, some of the
odg files contain multiple related versions of a figure, particularly feed forward neural
networks. The neural network of chap 5 can be found in one of the odg files of chap 4.  

A copy of this repository can be found in the following links:  

          https://bitbucket.org/nicolas-navarro-guerrero/thesis-nng-uhh-drawings  
          https://gitlab.com/nicolas-navarro-guerrero/thesis-nng-uhh-drawings  

==============================
#### Citing this Thesis ####

> Neurocomputational Mechanisms for Adaptive Self-Preservative Robot Behaviour  
> Universität Hamburg. Hamburg, Germany, May 2016.  
> Nicolás Navarro-Guerrero;   
> http://ediss.sub.uni-hamburg.de/volltexte/2016/7890/  

[Download the corresponding bib file](https://dl.dropboxusercontent.com/u/118126704/publications/2016-05-Navarro-Guerrero2016Neurocomputational.bib)

==============================
#### File structure: ####
 * drawings:  drawings and photos authored by me
 * images:    images authored by others by with license

============================================================
### LICENSE:
The collection of source files and the images is this repository are work by Nicolás Navarro-Guerrero and are licensed under the Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0). To view a copy of this license, visit https://creativecommons.org/licenses/by-sa/4.0/
